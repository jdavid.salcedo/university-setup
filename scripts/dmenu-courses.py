#!/usr/bin/python3
from Dmenu import dmenu
from courses import Courses

courses = Courses()
current = courses.current

try:
    current_index = courses.index(current)
    args = ['-a', current_index]
except ValueError:
    args = []

code, index, selected = dmenu([c.info['title'] for c in courses], centre=True, lines=len(courses), prompt='Select course')

if index >= 0:
    courses.current = courses[index]
